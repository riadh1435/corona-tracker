import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'cases-status',
        loadChildren: './cases-status/cases-status.module#CoronaTrackerCasesStatusModule'
      },
      {
        path: 'country',
        loadChildren: './country/country.module#CoronaTrackerCountryModule'
      },
      {
        path: 'criteria',
        loadChildren: './criteria/criteria.module#CoronaTrackerCriteriaModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoronaTrackerEntityModule {}
