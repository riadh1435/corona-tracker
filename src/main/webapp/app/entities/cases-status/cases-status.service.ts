import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICasesStatus } from 'app/shared/model/cases-status.model';

type EntityResponseType = HttpResponse<ICasesStatus>;
type EntityArrayResponseType = HttpResponse<ICasesStatus[]>;

@Injectable({ providedIn: 'root' })
export class CasesStatusService {
  public headerDict = {
    'Subscription-Key': '3009d4ccc29e4808af1ccc25c69b4d5d'
  };

  public requestOptions = {
    headers: new Headers(this.headerDict)
  };
  public resourceUrl = SERVER_API_URL + 'api/cases-statuses';

  constructor(protected http: HttpClient) {}

  create(casesStatus: ICasesStatus): Observable<EntityResponseType> {
    return this.http.post<ICasesStatus>(this.resourceUrl, casesStatus, { observe: 'response' });
  }

  update(casesStatus: ICasesStatus): Observable<EntityResponseType> {
    return this.http.put<ICasesStatus>(this.resourceUrl, casesStatus, { observe: 'response' });
  }

  findh(id: string): Observable<EntityResponseType> {
    return this.http.get<any>(`https://corona.lmao.ninja/v2/historical/${id}?lastdays=30`, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<any>(`https://api.smartable.ai/coronavirus/stats/${id}`, { observe: 'response', headers: this.headerDict });
  }

  find2(id: string): Observable<EntityResponseType> {
    return this.http.get<any>(`https://thevirustracker.com/free-api?countryTimeline=${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<any> {
    const options = createRequestOption(req);
    return this.http.get<any[]>('https://api.covid19api.com/summary');
    // return this.http.get<any[]>('https://corona-virus-stats.herokuapp.com/api/v1/cases/countries-search?limit=1000&page=1');
  }

  findByDate(date?: string): Observable<any> {
    // return this.http.get<any>(`https://thevirustracker.com/free-api?countryTimeline=${date}`, { observe: 'response' });
    return this.http.get<any[]>(`https://covid-api.com/api/reports?date=${date}`);
  }

  delete(id: string): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
