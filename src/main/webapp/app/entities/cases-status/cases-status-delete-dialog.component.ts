import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICasesStatus } from 'app/shared/model/cases-status.model';
import { CasesStatusService } from './cases-status.service';

@Component({
  selector: 'jhi-cases-status-delete-dialog',
  templateUrl: './cases-status-delete-dialog.component.html'
})
export class CasesStatusDeleteDialogComponent {
  casesStatus: ICasesStatus;

  constructor(
    protected casesStatusService: CasesStatusService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: string) {
    this.casesStatusService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'casesStatusListModification',
        content: 'Deleted an casesStatus'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-cases-status-delete-popup',
  template: ''
})
export class CasesStatusDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ casesStatus }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CasesStatusDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.casesStatus = casesStatus;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/cases-status', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/cases-status', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
