import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ICasesStatus } from 'app/shared/model/cases-status.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { CasesStatusService } from './cases-status.service';
import { FormBuilder } from '@angular/forms';
import { formatDate } from '@angular/common';
import * as XLSX from 'xlsx';

@Component({
  selector: 'jhi-cases-status-date',
  templateUrl: './cases-status-date.component.html'
})
export class CasesStatusDateComponent implements OnInit, OnDestroy {
  casesStatuses: any[];
  currentAccount: any;
  eventSubscriber: Subscription;
  itemsPerPage: number;
  links: any;
  page: any;
  predicate: any;
  reverse: any;
  totalItems: number;
  fileName = 'ExcelSheet.xlsx';

  editForm = this.fb.group({
    specificDate: []
  });

  constructor(
    protected casesStatusService: CasesStatusService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected parseLinks: JhiParseLinks,
    protected accountService: AccountService,
    private fb: FormBuilder
  ) {
    this.casesStatuses = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.reverse = true;
  }

  loadAll() {
    this.casesStatuses = [];
    const format = 'yyyy-MM-dd';
    const myDate = '2020-06-29';
    const locale = 'en-US';
    const date = this.editForm.get('specificDate').value;
    const formattedDate = date === null ? formatDate(myDate, format, locale) : date;
    this.casesStatusService
      .findByDate(formattedDate)
      .subscribe((res: any) => this.paginateCasesStatuses(res.data), (res: HttpErrorResponse) => this.onError(res.message));
  }

  reset() {
    this.page = 0;
    this.casesStatuses = [];
    this.loadAll();
  }

  loadPage(page) {
    this.page = page;
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInCasesStatuses();
  }

  exportexcel(): void {
    /* table id is passed over here */

    const element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICasesStatus) {
    return item.id;
  }

  registerChangeInCasesStatuses() {
    this.eventSubscriber = this.eventManager.subscribe('casesStatusListModification', response => this.reset());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateCasesStatuses(data: any[]) {
    for (let i = 0; i < data.length; i++) {
      this.casesStatuses.push(data[i]);
    }
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
