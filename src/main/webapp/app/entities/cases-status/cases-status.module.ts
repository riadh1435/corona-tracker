import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CoronaTrackerSharedModule } from 'app/shared';
import {
  CasesStatusComponent,
  CasesStatusDetailComponent,
  CasesStatusUpdateComponent,
  CasesStatusDeletePopupComponent,
  CasesStatusDeleteDialogComponent,
  casesStatusRoute,
  casesStatusPopupRoute
} from './';
import { CasesStatusDateComponent } from './cases-status-date.component';
import { ReactiveFormsModule } from '@angular/forms';

const ENTITY_STATES = [...casesStatusRoute, ...casesStatusPopupRoute];

@NgModule({
  imports: [CoronaTrackerSharedModule, RouterModule.forChild(ENTITY_STATES), ReactiveFormsModule],
  declarations: [
    CasesStatusComponent,
    CasesStatusDateComponent,
    CasesStatusDetailComponent,
    CasesStatusUpdateComponent,
    CasesStatusDeleteDialogComponent,
    CasesStatusDeletePopupComponent
  ],
  entryComponents: [
    CasesStatusComponent,
    CasesStatusDateComponent,
    CasesStatusUpdateComponent,
    CasesStatusDeleteDialogComponent,
    CasesStatusDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoronaTrackerCasesStatusModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
