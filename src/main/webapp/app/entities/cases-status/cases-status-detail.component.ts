import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICasesStatus } from 'app/shared/model/cases-status.model';

@Component({
  selector: 'jhi-cases-status-detail',
  templateUrl: './cases-status-detail.component.html'
})
export class CasesStatusDetailComponent implements OnInit {
  casesStatus: ICasesStatus;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ casesStatus }) => {
      this.casesStatus = casesStatus;
    });
  }

  previousState() {
    window.history.back();
  }
}
