import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CasesStatus } from 'app/shared/model/cases-status.model';
import { CasesStatusService } from './cases-status.service';
import { CasesStatusComponent } from './cases-status.component';
import { CasesStatusDetailComponent } from './cases-status-detail.component';
import { CasesStatusUpdateComponent } from './cases-status-update.component';
import { CasesStatusDeletePopupComponent } from './cases-status-delete-dialog.component';
import { ICasesStatus } from 'app/shared/model/cases-status.model';
import { CasesStatusDateComponent } from './cases-status-date.component';

@Injectable({ providedIn: 'root' })
export class CasesStatusResolve implements Resolve<ICasesStatus> {
  constructor(private service: CasesStatusService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICasesStatus> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CasesStatus>) => response.ok),
        map((casesStatus: HttpResponse<CasesStatus>) => casesStatus.body)
      );
    }
    return of(new CasesStatus());
  }
}

export const casesStatusRoute: Routes = [
  {
    path: '',
    component: CasesStatusComponent,
    data: {
      authorities: [],
      pageTitle: 'coronaTrackerApp.casesStatus.home.title'
    }
  },
  {
    path: 'date',
    component: CasesStatusDateComponent,
    data: {
      authorities: [],
      pageTitle: 'coronaTrackerApp.casesStatus.home.title'
    }
  },
  {
    path: ':id/view',
    component: CasesStatusDetailComponent,
    resolve: {
      casesStatus: CasesStatusResolve
    },
    data: {
      authorities: [],
      pageTitle: 'coronaTrackerApp.casesStatus.home.title'
    }
  },
  {
    path: 'new',
    component: CasesStatusUpdateComponent,
    resolve: {
      casesStatus: CasesStatusResolve
    },
    data: {
      authorities: [],
      pageTitle: 'coronaTrackerApp.casesStatus.home.title'
    }
  },
  {
    path: ':id/edit',
    component: CasesStatusUpdateComponent,
    resolve: {
      casesStatus: CasesStatusResolve
    },
    data: {
      authorities: [],
      pageTitle: 'coronaTrackerApp.casesStatus.home.title'
    }
  }
];

export const casesStatusPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CasesStatusDeletePopupComponent,
    resolve: {
      casesStatus: CasesStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'coronaTrackerApp.casesStatus.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
