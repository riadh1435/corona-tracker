import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ICasesStatus, CasesStatus } from 'app/shared/model/cases-status.model';
import { CasesStatusService } from './cases-status.service';

@Component({
  selector: 'jhi-cases-status-update',
  templateUrl: './cases-status-update.component.html'
})
export class CasesStatusUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    country: [],
    countryAbbreviation: [],
    totalCases: [],
    newCases: [],
    totalDeaths: [],
    newDeaths: [],
    totalRecovered: [],
    activeCases: [],
    seriousCritical: [],
    casesPerMillPop: [],
    flag: []
  });

  constructor(protected casesStatusService: CasesStatusService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ casesStatus }) => {
      this.updateForm(casesStatus);
    });
  }

  updateForm(casesStatus: ICasesStatus) {
    this.editForm.patchValue({
      id: casesStatus.id,
      country: casesStatus.country,
      countryAbbreviation: casesStatus.countryAbbreviation,
      totalCases: casesStatus.totalCases,
      newCases: casesStatus.newCases,
      totalDeaths: casesStatus.totalDeaths,
      newDeaths: casesStatus.newDeaths,
      totalRecovered: casesStatus.totalRecovered,
      activeCases: casesStatus.activeCases,
      seriousCritical: casesStatus.seriousCritical,
      casesPerMillPop: casesStatus.casesPerMillPop,
      flag: casesStatus.flag
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const casesStatus = this.createFromForm();
    if (casesStatus.id !== undefined) {
      this.subscribeToSaveResponse(this.casesStatusService.update(casesStatus));
    } else {
      this.subscribeToSaveResponse(this.casesStatusService.create(casesStatus));
    }
  }

  private createFromForm(): ICasesStatus {
    return {
      ...new CasesStatus(),
      id: this.editForm.get(['id']).value,
      country: this.editForm.get(['country']).value,
      countryAbbreviation: this.editForm.get(['countryAbbreviation']).value,
      totalCases: this.editForm.get(['totalCases']).value,
      newCases: this.editForm.get(['newCases']).value,
      totalDeaths: this.editForm.get(['totalDeaths']).value,
      newDeaths: this.editForm.get(['newDeaths']).value,
      totalRecovered: this.editForm.get(['totalRecovered']).value,
      activeCases: this.editForm.get(['activeCases']).value,
      seriousCritical: this.editForm.get(['seriousCritical']).value,
      casesPerMillPop: this.editForm.get(['casesPerMillPop']).value,
      flag: this.editForm.get(['flag']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICasesStatus>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
