import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CoronaTrackerSharedModule } from 'app/shared';
import {
  CriteriaComponent,
  CriteriaDetailComponent,
  CriteriaUpdateComponent,
  CriteriaDeletePopupComponent,
  CriteriaDeleteDialogComponent,
  criteriaRoute,
  criteriaPopupRoute
} from './';

const ENTITY_STATES = [...criteriaRoute, ...criteriaPopupRoute];

@NgModule({
  imports: [CoronaTrackerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CriteriaComponent,
    CriteriaDetailComponent,
    CriteriaUpdateComponent,
    CriteriaDeleteDialogComponent,
    CriteriaDeletePopupComponent
  ],
  entryComponents: [CriteriaComponent, CriteriaUpdateComponent, CriteriaDeleteDialogComponent, CriteriaDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoronaTrackerCriteriaModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
