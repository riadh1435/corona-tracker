import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CoronaTrackerSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [CoronaTrackerSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [CoronaTrackerSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoronaTrackerSharedModule {
  static forRoot() {
    return {
      ngModule: CoronaTrackerSharedModule
    };
  }
}
