export interface ICasesStatus {
  id?: string;
  country?: string;
  countryAbbreviation?: string;
  totalCases?: string;
  newCases?: string;
  totalDeaths?: string;
  newDeaths?: string;
  totalRecovered?: string;
  activeCases?: string;
  seriousCritical?: string;
  casesPerMillPop?: string;
  flag?: string;
}

export class CasesStatus implements ICasesStatus {
  constructor(
    public id?: string,
    public country?: string,
    public countryAbbreviation?: string,
    public totalCases?: string,
    public newCases?: string,
    public totalDeaths?: string,
    public newDeaths?: string,
    public totalRecovered?: string,
    public activeCases?: string,
    public seriousCritical?: string,
    public casesPerMillPop?: string,
    public flag?: string
  ) {}
}
