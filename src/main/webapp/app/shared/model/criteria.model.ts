import { ICountry } from 'app/shared/model/country.model';

export interface ICriteria {
  id?: string;
  name?: string;
  value?: string;
  country?: ICountry;
}

export class Criteria implements ICriteria {
  constructor(public id?: string, public name?: string, public value?: string, public country?: ICountry) {}
}
