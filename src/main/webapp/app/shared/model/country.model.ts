import { ICriteria } from 'app/shared/model/criteria.model';

export interface ICountry {
  id?: string;
  countryName?: string;
  countryAbbreviation?: string;
  criteria?: ICriteria[];
}

export class Country implements ICountry {
  constructor(public id?: string, public countryName?: string, public countryAbbreviation?: string, public criteria?: ICriteria[]) {}
}
