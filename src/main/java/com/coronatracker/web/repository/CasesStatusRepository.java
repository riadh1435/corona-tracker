package com.coronatracker.web.repository;

import com.coronatracker.web.domain.CasesStatus;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the CasesStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CasesStatusRepository extends MongoRepository<CasesStatus, String> {

}
