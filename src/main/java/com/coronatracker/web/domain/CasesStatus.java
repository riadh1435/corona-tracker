package com.coronatracker.web.domain;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * A CasesStatus.
 */
@Document(collection = "cases_status")
public class CasesStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("country")
    private String country;

    @Field("country_abbreviation")
    private String countryAbbreviation;

    @Field("total_cases")
    private String totalCases;

    @Field("new_cases")
    private String newCases;

    @Field("total_deaths")
    private String totalDeaths;

    @Field("new_deaths")
    private String newDeaths;

    @Field("total_recovered")
    private String totalRecovered;

    @Field("active_cases")
    private String activeCases;

    @Field("serious_critical")
    private String seriousCritical;

    @Field("cases_per_mill_pop")
    private String casesPerMillPop;

    @Field("flag")
    private String flag;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public CasesStatus country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryAbbreviation() {
        return countryAbbreviation;
    }

    public CasesStatus countryAbbreviation(String countryAbbreviation) {
        this.countryAbbreviation = countryAbbreviation;
        return this;
    }

    public void setCountryAbbreviation(String countryAbbreviation) {
        this.countryAbbreviation = countryAbbreviation;
    }

    public String getTotalCases() {
        return totalCases;
    }

    public CasesStatus totalCases(String totalCases) {
        this.totalCases = totalCases;
        return this;
    }

    public void setTotalCases(String totalCases) {
        this.totalCases = totalCases;
    }

    public String getNewCases() {
        return newCases;
    }

    public CasesStatus newCases(String newCases) {
        this.newCases = newCases;
        return this;
    }

    public void setNewCases(String newCases) {
        this.newCases = newCases;
    }

    public String getTotalDeaths() {
        return totalDeaths;
    }

    public CasesStatus totalDeaths(String totalDeaths) {
        this.totalDeaths = totalDeaths;
        return this;
    }

    public void setTotalDeaths(String totalDeaths) {
        this.totalDeaths = totalDeaths;
    }

    public String getNewDeaths() {
        return newDeaths;
    }

    public CasesStatus newDeaths(String newDeaths) {
        this.newDeaths = newDeaths;
        return this;
    }

    public void setNewDeaths(String newDeaths) {
        this.newDeaths = newDeaths;
    }

    public String getTotalRecovered() {
        return totalRecovered;
    }

    public CasesStatus totalRecovered(String totalRecovered) {
        this.totalRecovered = totalRecovered;
        return this;
    }

    public void setTotalRecovered(String totalRecovered) {
        this.totalRecovered = totalRecovered;
    }

    public String getActiveCases() {
        return activeCases;
    }

    public CasesStatus activeCases(String activeCases) {
        this.activeCases = activeCases;
        return this;
    }

    public void setActiveCases(String activeCases) {
        this.activeCases = activeCases;
    }

    public String getSeriousCritical() {
        return seriousCritical;
    }

    public CasesStatus seriousCritical(String seriousCritical) {
        this.seriousCritical = seriousCritical;
        return this;
    }

    public void setSeriousCritical(String seriousCritical) {
        this.seriousCritical = seriousCritical;
    }

    public String getCasesPerMillPop() {
        return casesPerMillPop;
    }

    public CasesStatus casesPerMillPop(String casesPerMillPop) {
        this.casesPerMillPop = casesPerMillPop;
        return this;
    }

    public void setCasesPerMillPop(String casesPerMillPop) {
        this.casesPerMillPop = casesPerMillPop;
    }

    public String getFlag() {
        return flag;
    }

    public CasesStatus flag(String flag) {
        this.flag = flag;
        return this;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CasesStatus)) {
            return false;
        }
        return id != null && id.equals(((CasesStatus) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CasesStatus{" +
            "id=" + getId() +
            ", country='" + getCountry() + "'" +
            ", countryAbbreviation='" + getCountryAbbreviation() + "'" +
            ", totalCases='" + getTotalCases() + "'" +
            ", newCases='" + getNewCases() + "'" +
            ", totalDeaths='" + getTotalDeaths() + "'" +
            ", newDeaths='" + getNewDeaths() + "'" +
            ", totalRecovered='" + getTotalRecovered() + "'" +
            ", activeCases='" + getActiveCases() + "'" +
            ", seriousCritical='" + getSeriousCritical() + "'" +
            ", casesPerMillPop='" + getCasesPerMillPop() + "'" +
            ", flag='" + getFlag() + "'" +
            "}";
    }
}
