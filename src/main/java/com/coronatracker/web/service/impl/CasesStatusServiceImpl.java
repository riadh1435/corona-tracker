package com.coronatracker.web.service.impl;

import com.coronatracker.web.service.CasesStatusService;
import com.coronatracker.web.domain.CasesStatus;
import com.coronatracker.web.repository.CasesStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CasesStatus}.
 */
@Service
public class CasesStatusServiceImpl implements CasesStatusService {

    private final Logger log = LoggerFactory.getLogger(CasesStatusServiceImpl.class);

    private final CasesStatusRepository casesStatusRepository;

    public CasesStatusServiceImpl(CasesStatusRepository casesStatusRepository) {
        this.casesStatusRepository = casesStatusRepository;
    }

    /**
     * Save a casesStatus.
     *
     * @param casesStatus the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CasesStatus save(CasesStatus casesStatus) {
        log.debug("Request to save CasesStatus : {}", casesStatus);
        return casesStatusRepository.save(casesStatus);
    }

    /**
     * Get all the casesStatuses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    public Page<CasesStatus> findAll(Pageable pageable) {
        log.debug("Request to get all CasesStatuses");
        return casesStatusRepository.findAll(pageable);
    }


    /**
     * Get one casesStatus by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<CasesStatus> findOne(String id) {
        log.debug("Request to get CasesStatus : {}", id);
        return casesStatusRepository.findById(id);
    }

    /**
     * Delete the casesStatus by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete CasesStatus : {}", id);
        casesStatusRepository.deleteById(id);
    }
}
