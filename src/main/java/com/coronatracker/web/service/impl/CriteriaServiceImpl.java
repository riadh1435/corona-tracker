package com.coronatracker.web.service.impl;

import com.coronatracker.web.service.CriteriaService;
import com.coronatracker.web.domain.Criteria;
import com.coronatracker.web.repository.CriteriaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Criteria}.
 */
@Service
public class CriteriaServiceImpl implements CriteriaService {

    private final Logger log = LoggerFactory.getLogger(CriteriaServiceImpl.class);

    private final CriteriaRepository criteriaRepository;

    public CriteriaServiceImpl(CriteriaRepository criteriaRepository) {
        this.criteriaRepository = criteriaRepository;
    }

    /**
     * Save a criteria.
     *
     * @param criteria the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Criteria save(Criteria criteria) {
        log.debug("Request to save Criteria : {}", criteria);
        return criteriaRepository.save(criteria);
    }

    /**
     * Get all the criteria.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    public Page<Criteria> findAll(Pageable pageable) {
        log.debug("Request to get all Criteria");
        return criteriaRepository.findAll(pageable);
    }


    /**
     * Get one criteria by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<Criteria> findOne(String id) {
        log.debug("Request to get Criteria : {}", id);
        return criteriaRepository.findById(id);
    }

    /**
     * Delete the criteria by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Criteria : {}", id);
        criteriaRepository.deleteById(id);
    }
}
