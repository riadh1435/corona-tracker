package com.coronatracker.web.service;

import com.coronatracker.web.domain.Criteria;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Criteria}.
 */
public interface CriteriaService {

    /**
     * Save a criteria.
     *
     * @param criteria the entity to save.
     * @return the persisted entity.
     */
    Criteria save(Criteria criteria);

    /**
     * Get all the criteria.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Criteria> findAll(Pageable pageable);


    /**
     * Get the "id" criteria.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Criteria> findOne(String id);

    /**
     * Delete the "id" criteria.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
