package com.coronatracker.web.service;

import com.coronatracker.web.domain.CasesStatus;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link CasesStatus}.
 */
public interface CasesStatusService {

    /**
     * Save a casesStatus.
     *
     * @param casesStatus the entity to save.
     * @return the persisted entity.
     */
    CasesStatus save(CasesStatus casesStatus);

    /**
     * Get all the casesStatuses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CasesStatus> findAll(Pageable pageable);


    /**
     * Get the "id" casesStatus.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CasesStatus> findOne(String id);

    /**
     * Delete the "id" casesStatus.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
