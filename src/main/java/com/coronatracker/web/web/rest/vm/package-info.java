/**
 * View Models used by Spring MVC REST controllers.
 */
package com.coronatracker.web.web.rest.vm;
