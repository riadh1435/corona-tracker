package com.coronatracker.web.web.rest;

import com.coronatracker.web.domain.CasesStatus;
import com.coronatracker.web.service.CasesStatusService;
import com.coronatracker.web.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.coronatracker.web.domain.CasesStatus}.
 */
@RestController
@RequestMapping("/api")
public class CasesStatusResource {

    private final Logger log = LoggerFactory.getLogger(CasesStatusResource.class);

    private static final String ENTITY_NAME = "casesStatus";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CasesStatusService casesStatusService;

    public CasesStatusResource(CasesStatusService casesStatusService) {
        this.casesStatusService = casesStatusService;
    }

    /**
     * {@code POST  /cases-statuses} : Create a new casesStatus.
     *
     * @param casesStatus the casesStatus to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new casesStatus, or with status {@code 400 (Bad Request)} if the casesStatus has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cases-statuses")
    public ResponseEntity<CasesStatus> createCasesStatus(@RequestBody CasesStatus casesStatus) throws URISyntaxException {
        log.debug("REST request to save CasesStatus : {}", casesStatus);
        if (casesStatus.getId() != null) {
            throw new BadRequestAlertException("A new casesStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CasesStatus result = casesStatusService.save(casesStatus);
        return ResponseEntity.created(new URI("/api/cases-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cases-statuses} : Updates an existing casesStatus.
     *
     * @param casesStatus the casesStatus to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated casesStatus,
     * or with status {@code 400 (Bad Request)} if the casesStatus is not valid,
     * or with status {@code 500 (Internal Server Error)} if the casesStatus couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cases-statuses")
    public ResponseEntity<CasesStatus> updateCasesStatus(@RequestBody CasesStatus casesStatus) throws URISyntaxException {
        log.debug("REST request to update CasesStatus : {}", casesStatus);
        if (casesStatus.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CasesStatus result = casesStatusService.save(casesStatus);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, casesStatus.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cases-statuses} : get all the casesStatuses.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of casesStatuses in body.
     */
    @GetMapping("/cases-statuses")
    public ResponseEntity<List<CasesStatus>> getAllCasesStatuses(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of CasesStatuses");
        Page<CasesStatus> page = casesStatusService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cases-statuses/:id} : get the "id" casesStatus.
     *
     * @param id the id of the casesStatus to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the casesStatus, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cases-statuses/{id}")
    public ResponseEntity<CasesStatus> getCasesStatus(@PathVariable String id) {
        log.debug("REST request to get CasesStatus : {}", id);
        Optional<CasesStatus> casesStatus = casesStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(casesStatus);
    }

    /**
     * {@code DELETE  /cases-statuses/:id} : delete the "id" casesStatus.
     *
     * @param id the id of the casesStatus to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cases-statuses/{id}")
    public ResponseEntity<Void> deleteCasesStatus(@PathVariable String id) {
        log.debug("REST request to delete CasesStatus : {}", id);
        casesStatusService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
