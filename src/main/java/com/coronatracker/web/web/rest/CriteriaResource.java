package com.coronatracker.web.web.rest;

import com.coronatracker.web.domain.Criteria;
import com.coronatracker.web.service.CriteriaService;
import com.coronatracker.web.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.coronatracker.web.domain.Criteria}.
 */
@RestController
@RequestMapping("/api")
public class CriteriaResource {

    private final Logger log = LoggerFactory.getLogger(CriteriaResource.class);

    private static final String ENTITY_NAME = "criteria";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CriteriaService criteriaService;

    public CriteriaResource(CriteriaService criteriaService) {
        this.criteriaService = criteriaService;
    }

    /**
     * {@code POST  /criteria} : Create a new criteria.
     *
     * @param criteria the criteria to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new criteria, or with status {@code 400 (Bad Request)} if the criteria has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/criteria")
    public ResponseEntity<Criteria> createCriteria(@RequestBody Criteria criteria) throws URISyntaxException {
        log.debug("REST request to save Criteria : {}", criteria);
        if (criteria.getId() != null) {
            throw new BadRequestAlertException("A new criteria cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Criteria result = criteriaService.save(criteria);
        return ResponseEntity.created(new URI("/api/criteria/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /criteria} : Updates an existing criteria.
     *
     * @param criteria the criteria to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated criteria,
     * or with status {@code 400 (Bad Request)} if the criteria is not valid,
     * or with status {@code 500 (Internal Server Error)} if the criteria couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/criteria")
    public ResponseEntity<Criteria> updateCriteria(@RequestBody Criteria criteria) throws URISyntaxException {
        log.debug("REST request to update Criteria : {}", criteria);
        if (criteria.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Criteria result = criteriaService.save(criteria);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, criteria.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /criteria} : get all the criteria.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of criteria in body.
     */
    @GetMapping("/criteria")
    public ResponseEntity<List<Criteria>> getAllCriteria(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Criteria");
        Page<Criteria> page = criteriaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /criteria/:id} : get the "id" criteria.
     *
     * @param id the id of the criteria to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the criteria, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/criteria/{id}")
    public ResponseEntity<Criteria> getCriteria(@PathVariable String id) {
        log.debug("REST request to get Criteria : {}", id);
        Optional<Criteria> criteria = criteriaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(criteria);
    }

    /**
     * {@code DELETE  /criteria/:id} : delete the "id" criteria.
     *
     * @param id the id of the criteria to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/criteria/{id}")
    public ResponseEntity<Void> deleteCriteria(@PathVariable String id) {
        log.debug("REST request to delete Criteria : {}", id);
        criteriaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
