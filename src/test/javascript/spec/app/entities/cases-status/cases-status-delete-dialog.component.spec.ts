/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CoronaTrackerTestModule } from '../../../test.module';
import { CasesStatusDeleteDialogComponent } from 'app/entities/cases-status/cases-status-delete-dialog.component';
import { CasesStatusService } from 'app/entities/cases-status/cases-status.service';

describe('Component Tests', () => {
  describe('CasesStatus Management Delete Component', () => {
    let comp: CasesStatusDeleteDialogComponent;
    let fixture: ComponentFixture<CasesStatusDeleteDialogComponent>;
    let service: CasesStatusService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoronaTrackerTestModule],
        declarations: [CasesStatusDeleteDialogComponent]
      })
        .overrideTemplate(CasesStatusDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CasesStatusDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CasesStatusService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete('123');
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith('123');
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
