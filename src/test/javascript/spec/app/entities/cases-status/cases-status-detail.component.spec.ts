/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoronaTrackerTestModule } from '../../../test.module';
import { CasesStatusDetailComponent } from 'app/entities/cases-status/cases-status-detail.component';
import { CasesStatus } from 'app/shared/model/cases-status.model';

describe('Component Tests', () => {
  describe('CasesStatus Management Detail Component', () => {
    let comp: CasesStatusDetailComponent;
    let fixture: ComponentFixture<CasesStatusDetailComponent>;
    const route = ({ data: of({ casesStatus: new CasesStatus('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoronaTrackerTestModule],
        declarations: [CasesStatusDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CasesStatusDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CasesStatusDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.casesStatus).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
