package com.coronatracker.web.web.rest;

import com.coronatracker.web.CoronaTrackerApp;
import com.coronatracker.web.domain.CasesStatus;
import com.coronatracker.web.repository.CasesStatusRepository;
import com.coronatracker.web.service.CasesStatusService;
import com.coronatracker.web.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;


import java.util.List;

import static com.coronatracker.web.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link CasesStatusResource} REST controller.
 */
@SpringBootTest(classes = CoronaTrackerApp.class)
public class CasesStatusResourceIT {

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY_ABBREVIATION = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_ABBREVIATION = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_CASES = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_CASES = "BBBBBBBBBB";

    private static final String DEFAULT_NEW_CASES = "AAAAAAAAAA";
    private static final String UPDATED_NEW_CASES = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_DEATHS = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_DEATHS = "BBBBBBBBBB";

    private static final String DEFAULT_NEW_DEATHS = "AAAAAAAAAA";
    private static final String UPDATED_NEW_DEATHS = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_RECOVERED = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_RECOVERED = "BBBBBBBBBB";

    private static final String DEFAULT_ACTIVE_CASES = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVE_CASES = "BBBBBBBBBB";

    private static final String DEFAULT_SERIOUS_CRITICAL = "AAAAAAAAAA";
    private static final String UPDATED_SERIOUS_CRITICAL = "BBBBBBBBBB";

    private static final String DEFAULT_CASES_PER_MILL_POP = "AAAAAAAAAA";
    private static final String UPDATED_CASES_PER_MILL_POP = "BBBBBBBBBB";

    private static final String DEFAULT_FLAG = "AAAAAAAAAA";
    private static final String UPDATED_FLAG = "BBBBBBBBBB";

    @Autowired
    private CasesStatusRepository casesStatusRepository;

    @Autowired
    private CasesStatusService casesStatusService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restCasesStatusMockMvc;

    private CasesStatus casesStatus;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CasesStatusResource casesStatusResource = new CasesStatusResource(casesStatusService);
        this.restCasesStatusMockMvc = MockMvcBuilders.standaloneSetup(casesStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CasesStatus createEntity() {
        CasesStatus casesStatus = new CasesStatus()
            .country(DEFAULT_COUNTRY)
            .countryAbbreviation(DEFAULT_COUNTRY_ABBREVIATION)
            .totalCases(DEFAULT_TOTAL_CASES)
            .newCases(DEFAULT_NEW_CASES)
            .totalDeaths(DEFAULT_TOTAL_DEATHS)
            .newDeaths(DEFAULT_NEW_DEATHS)
            .totalRecovered(DEFAULT_TOTAL_RECOVERED)
            .activeCases(DEFAULT_ACTIVE_CASES)
            .seriousCritical(DEFAULT_SERIOUS_CRITICAL)
            .casesPerMillPop(DEFAULT_CASES_PER_MILL_POP)
            .flag(DEFAULT_FLAG);
        return casesStatus;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CasesStatus createUpdatedEntity() {
        CasesStatus casesStatus = new CasesStatus()
            .country(UPDATED_COUNTRY)
            .countryAbbreviation(UPDATED_COUNTRY_ABBREVIATION)
            .totalCases(UPDATED_TOTAL_CASES)
            .newCases(UPDATED_NEW_CASES)
            .totalDeaths(UPDATED_TOTAL_DEATHS)
            .newDeaths(UPDATED_NEW_DEATHS)
            .totalRecovered(UPDATED_TOTAL_RECOVERED)
            .activeCases(UPDATED_ACTIVE_CASES)
            .seriousCritical(UPDATED_SERIOUS_CRITICAL)
            .casesPerMillPop(UPDATED_CASES_PER_MILL_POP)
            .flag(UPDATED_FLAG);
        return casesStatus;
    }

    @BeforeEach
    public void initTest() {
        casesStatusRepository.deleteAll();
        casesStatus = createEntity();
    }

    @Test
    public void createCasesStatus() throws Exception {
        int databaseSizeBeforeCreate = casesStatusRepository.findAll().size();

        // Create the CasesStatus
        restCasesStatusMockMvc.perform(post("/api/cases-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(casesStatus)))
            .andExpect(status().isCreated());

        // Validate the CasesStatus in the database
        List<CasesStatus> casesStatusList = casesStatusRepository.findAll();
        assertThat(casesStatusList).hasSize(databaseSizeBeforeCreate + 1);
        CasesStatus testCasesStatus = casesStatusList.get(casesStatusList.size() - 1);
        assertThat(testCasesStatus.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testCasesStatus.getCountryAbbreviation()).isEqualTo(DEFAULT_COUNTRY_ABBREVIATION);
        assertThat(testCasesStatus.getTotalCases()).isEqualTo(DEFAULT_TOTAL_CASES);
        assertThat(testCasesStatus.getNewCases()).isEqualTo(DEFAULT_NEW_CASES);
        assertThat(testCasesStatus.getTotalDeaths()).isEqualTo(DEFAULT_TOTAL_DEATHS);
        assertThat(testCasesStatus.getNewDeaths()).isEqualTo(DEFAULT_NEW_DEATHS);
        assertThat(testCasesStatus.getTotalRecovered()).isEqualTo(DEFAULT_TOTAL_RECOVERED);
        assertThat(testCasesStatus.getActiveCases()).isEqualTo(DEFAULT_ACTIVE_CASES);
        assertThat(testCasesStatus.getSeriousCritical()).isEqualTo(DEFAULT_SERIOUS_CRITICAL);
        assertThat(testCasesStatus.getCasesPerMillPop()).isEqualTo(DEFAULT_CASES_PER_MILL_POP);
        assertThat(testCasesStatus.getFlag()).isEqualTo(DEFAULT_FLAG);
    }

    @Test
    public void createCasesStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = casesStatusRepository.findAll().size();

        // Create the CasesStatus with an existing ID
        casesStatus.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCasesStatusMockMvc.perform(post("/api/cases-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(casesStatus)))
            .andExpect(status().isBadRequest());

        // Validate the CasesStatus in the database
        List<CasesStatus> casesStatusList = casesStatusRepository.findAll();
        assertThat(casesStatusList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCasesStatuses() throws Exception {
        // Initialize the database
        casesStatusRepository.save(casesStatus);

        // Get all the casesStatusList
        restCasesStatusMockMvc.perform(get("/api/cases-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(casesStatus.getId())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].countryAbbreviation").value(hasItem(DEFAULT_COUNTRY_ABBREVIATION.toString())))
            .andExpect(jsonPath("$.[*].totalCases").value(hasItem(DEFAULT_TOTAL_CASES.toString())))
            .andExpect(jsonPath("$.[*].newCases").value(hasItem(DEFAULT_NEW_CASES.toString())))
            .andExpect(jsonPath("$.[*].totalDeaths").value(hasItem(DEFAULT_TOTAL_DEATHS.toString())))
            .andExpect(jsonPath("$.[*].newDeaths").value(hasItem(DEFAULT_NEW_DEATHS.toString())))
            .andExpect(jsonPath("$.[*].totalRecovered").value(hasItem(DEFAULT_TOTAL_RECOVERED.toString())))
            .andExpect(jsonPath("$.[*].activeCases").value(hasItem(DEFAULT_ACTIVE_CASES.toString())))
            .andExpect(jsonPath("$.[*].seriousCritical").value(hasItem(DEFAULT_SERIOUS_CRITICAL.toString())))
            .andExpect(jsonPath("$.[*].casesPerMillPop").value(hasItem(DEFAULT_CASES_PER_MILL_POP.toString())))
            .andExpect(jsonPath("$.[*].flag").value(hasItem(DEFAULT_FLAG.toString())));
    }
    
    @Test
    public void getCasesStatus() throws Exception {
        // Initialize the database
        casesStatusRepository.save(casesStatus);

        // Get the casesStatus
        restCasesStatusMockMvc.perform(get("/api/cases-statuses/{id}", casesStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(casesStatus.getId()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.countryAbbreviation").value(DEFAULT_COUNTRY_ABBREVIATION.toString()))
            .andExpect(jsonPath("$.totalCases").value(DEFAULT_TOTAL_CASES.toString()))
            .andExpect(jsonPath("$.newCases").value(DEFAULT_NEW_CASES.toString()))
            .andExpect(jsonPath("$.totalDeaths").value(DEFAULT_TOTAL_DEATHS.toString()))
            .andExpect(jsonPath("$.newDeaths").value(DEFAULT_NEW_DEATHS.toString()))
            .andExpect(jsonPath("$.totalRecovered").value(DEFAULT_TOTAL_RECOVERED.toString()))
            .andExpect(jsonPath("$.activeCases").value(DEFAULT_ACTIVE_CASES.toString()))
            .andExpect(jsonPath("$.seriousCritical").value(DEFAULT_SERIOUS_CRITICAL.toString()))
            .andExpect(jsonPath("$.casesPerMillPop").value(DEFAULT_CASES_PER_MILL_POP.toString()))
            .andExpect(jsonPath("$.flag").value(DEFAULT_FLAG.toString()));
    }

    @Test
    public void getNonExistingCasesStatus() throws Exception {
        // Get the casesStatus
        restCasesStatusMockMvc.perform(get("/api/cases-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCasesStatus() throws Exception {
        // Initialize the database
        casesStatusService.save(casesStatus);

        int databaseSizeBeforeUpdate = casesStatusRepository.findAll().size();

        // Update the casesStatus
        CasesStatus updatedCasesStatus = casesStatusRepository.findById(casesStatus.getId()).get();
        updatedCasesStatus
            .country(UPDATED_COUNTRY)
            .countryAbbreviation(UPDATED_COUNTRY_ABBREVIATION)
            .totalCases(UPDATED_TOTAL_CASES)
            .newCases(UPDATED_NEW_CASES)
            .totalDeaths(UPDATED_TOTAL_DEATHS)
            .newDeaths(UPDATED_NEW_DEATHS)
            .totalRecovered(UPDATED_TOTAL_RECOVERED)
            .activeCases(UPDATED_ACTIVE_CASES)
            .seriousCritical(UPDATED_SERIOUS_CRITICAL)
            .casesPerMillPop(UPDATED_CASES_PER_MILL_POP)
            .flag(UPDATED_FLAG);

        restCasesStatusMockMvc.perform(put("/api/cases-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCasesStatus)))
            .andExpect(status().isOk());

        // Validate the CasesStatus in the database
        List<CasesStatus> casesStatusList = casesStatusRepository.findAll();
        assertThat(casesStatusList).hasSize(databaseSizeBeforeUpdate);
        CasesStatus testCasesStatus = casesStatusList.get(casesStatusList.size() - 1);
        assertThat(testCasesStatus.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testCasesStatus.getCountryAbbreviation()).isEqualTo(UPDATED_COUNTRY_ABBREVIATION);
        assertThat(testCasesStatus.getTotalCases()).isEqualTo(UPDATED_TOTAL_CASES);
        assertThat(testCasesStatus.getNewCases()).isEqualTo(UPDATED_NEW_CASES);
        assertThat(testCasesStatus.getTotalDeaths()).isEqualTo(UPDATED_TOTAL_DEATHS);
        assertThat(testCasesStatus.getNewDeaths()).isEqualTo(UPDATED_NEW_DEATHS);
        assertThat(testCasesStatus.getTotalRecovered()).isEqualTo(UPDATED_TOTAL_RECOVERED);
        assertThat(testCasesStatus.getActiveCases()).isEqualTo(UPDATED_ACTIVE_CASES);
        assertThat(testCasesStatus.getSeriousCritical()).isEqualTo(UPDATED_SERIOUS_CRITICAL);
        assertThat(testCasesStatus.getCasesPerMillPop()).isEqualTo(UPDATED_CASES_PER_MILL_POP);
        assertThat(testCasesStatus.getFlag()).isEqualTo(UPDATED_FLAG);
    }

    @Test
    public void updateNonExistingCasesStatus() throws Exception {
        int databaseSizeBeforeUpdate = casesStatusRepository.findAll().size();

        // Create the CasesStatus

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCasesStatusMockMvc.perform(put("/api/cases-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(casesStatus)))
            .andExpect(status().isBadRequest());

        // Validate the CasesStatus in the database
        List<CasesStatus> casesStatusList = casesStatusRepository.findAll();
        assertThat(casesStatusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCasesStatus() throws Exception {
        // Initialize the database
        casesStatusService.save(casesStatus);

        int databaseSizeBeforeDelete = casesStatusRepository.findAll().size();

        // Delete the casesStatus
        restCasesStatusMockMvc.perform(delete("/api/cases-statuses/{id}", casesStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CasesStatus> casesStatusList = casesStatusRepository.findAll();
        assertThat(casesStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CasesStatus.class);
        CasesStatus casesStatus1 = new CasesStatus();
        casesStatus1.setId("id1");
        CasesStatus casesStatus2 = new CasesStatus();
        casesStatus2.setId(casesStatus1.getId());
        assertThat(casesStatus1).isEqualTo(casesStatus2);
        casesStatus2.setId("id2");
        assertThat(casesStatus1).isNotEqualTo(casesStatus2);
        casesStatus1.setId(null);
        assertThat(casesStatus1).isNotEqualTo(casesStatus2);
    }
}
