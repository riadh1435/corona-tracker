package com.coronatracker.web.web.rest;

import com.coronatracker.web.CoronaTrackerApp;
import com.coronatracker.web.domain.Criteria;
import com.coronatracker.web.repository.CriteriaRepository;
import com.coronatracker.web.service.CriteriaService;
import com.coronatracker.web.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;


import java.util.List;

import static com.coronatracker.web.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link CriteriaResource} REST controller.
 */
@SpringBootTest(classes = CoronaTrackerApp.class)
public class CriteriaResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    @Autowired
    private CriteriaRepository criteriaRepository;

    @Autowired
    private CriteriaService criteriaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restCriteriaMockMvc;

    private Criteria criteria;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CriteriaResource criteriaResource = new CriteriaResource(criteriaService);
        this.restCriteriaMockMvc = MockMvcBuilders.standaloneSetup(criteriaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Criteria createEntity() {
        Criteria criteria = new Criteria()
            .name(DEFAULT_NAME)
            .value(DEFAULT_VALUE);
        return criteria;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Criteria createUpdatedEntity() {
        Criteria criteria = new Criteria()
            .name(UPDATED_NAME)
            .value(UPDATED_VALUE);
        return criteria;
    }

    @BeforeEach
    public void initTest() {
        criteriaRepository.deleteAll();
        criteria = createEntity();
    }

    @Test
    public void createCriteria() throws Exception {
        int databaseSizeBeforeCreate = criteriaRepository.findAll().size();

        // Create the Criteria
        restCriteriaMockMvc.perform(post("/api/criteria")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(criteria)))
            .andExpect(status().isCreated());

        // Validate the Criteria in the database
        List<Criteria> criteriaList = criteriaRepository.findAll();
        assertThat(criteriaList).hasSize(databaseSizeBeforeCreate + 1);
        Criteria testCriteria = criteriaList.get(criteriaList.size() - 1);
        assertThat(testCriteria.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCriteria.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    public void createCriteriaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = criteriaRepository.findAll().size();

        // Create the Criteria with an existing ID
        criteria.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCriteriaMockMvc.perform(post("/api/criteria")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(criteria)))
            .andExpect(status().isBadRequest());

        // Validate the Criteria in the database
        List<Criteria> criteriaList = criteriaRepository.findAll();
        assertThat(criteriaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCriteria() throws Exception {
        // Initialize the database
        criteriaRepository.save(criteria);

        // Get all the criteriaList
        restCriteriaMockMvc.perform(get("/api/criteria?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(criteria.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }
    
    @Test
    public void getCriteria() throws Exception {
        // Initialize the database
        criteriaRepository.save(criteria);

        // Get the criteria
        restCriteriaMockMvc.perform(get("/api/criteria/{id}", criteria.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(criteria.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    public void getNonExistingCriteria() throws Exception {
        // Get the criteria
        restCriteriaMockMvc.perform(get("/api/criteria/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCriteria() throws Exception {
        // Initialize the database
        criteriaService.save(criteria);

        int databaseSizeBeforeUpdate = criteriaRepository.findAll().size();

        // Update the criteria
        Criteria updatedCriteria = criteriaRepository.findById(criteria.getId()).get();
        updatedCriteria
            .name(UPDATED_NAME)
            .value(UPDATED_VALUE);

        restCriteriaMockMvc.perform(put("/api/criteria")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCriteria)))
            .andExpect(status().isOk());

        // Validate the Criteria in the database
        List<Criteria> criteriaList = criteriaRepository.findAll();
        assertThat(criteriaList).hasSize(databaseSizeBeforeUpdate);
        Criteria testCriteria = criteriaList.get(criteriaList.size() - 1);
        assertThat(testCriteria.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCriteria.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    public void updateNonExistingCriteria() throws Exception {
        int databaseSizeBeforeUpdate = criteriaRepository.findAll().size();

        // Create the Criteria

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCriteriaMockMvc.perform(put("/api/criteria")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(criteria)))
            .andExpect(status().isBadRequest());

        // Validate the Criteria in the database
        List<Criteria> criteriaList = criteriaRepository.findAll();
        assertThat(criteriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCriteria() throws Exception {
        // Initialize the database
        criteriaService.save(criteria);

        int databaseSizeBeforeDelete = criteriaRepository.findAll().size();

        // Delete the criteria
        restCriteriaMockMvc.perform(delete("/api/criteria/{id}", criteria.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Criteria> criteriaList = criteriaRepository.findAll();
        assertThat(criteriaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Criteria.class);
        Criteria criteria1 = new Criteria();
        criteria1.setId("id1");
        Criteria criteria2 = new Criteria();
        criteria2.setId(criteria1.getId());
        assertThat(criteria1).isEqualTo(criteria2);
        criteria2.setId("id2");
        assertThat(criteria1).isNotEqualTo(criteria2);
        criteria1.setId(null);
        assertThat(criteria1).isNotEqualTo(criteria2);
    }
}
